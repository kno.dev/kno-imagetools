KNOCONFIG         = knoconfig
KNOBUILD          = knobuild
INSTALL_SCOPE     = shared

prefix		 := $(shell ${KNOCONFIG} prefix)
libsuffix	 := $(shell ${KNOCONFIG} libsuffix)
CMODULES	 := $(DESTDIR)$(shell ${KNOCONFIG} cmodules $(INSTALL_SCOPE))
KNO_CFLAGS	 := -I. -fPIC $(shell ${KNOCONFIG} cflags)
KNO_LDFLAGS	 := -fPIC $(shell ${KNOCONFIG} ldflags)
KNO_LIBS	 := $(shell ${KNOCONFIG} libs)
PACKAGE_CFLAGS   := $(shell etc/pkc --cflags libexif) \
		    $(shell etc/pkc --cflags libqrencode) \
		    $(shell etc/pkc --cflags libpng) \
		    $(shell etc/pkc --cflags ImageMagick) \
		    $(shell etc/pkc --cflags MagickWand)
PACKAGE_LDFLAGS  := $(shell etc/pkc --libs libexif) \
		    $(shell etc/pkc --libs libqrencode) \
		    $(shell etc/pkc --libs libpng) \
		    $(shell etc/pkc --libs ImageMagick) \
		    $(shell etc/pkc --libs MagickWand)
LIBS		 := $(shell ${KNOCONFIG} libs)
LIB		 := $(shell ${KNOCONFIG} lib)
INCLUDE		 := $(shell ${KNOCONFIG} include)
KNO_VERSION	 := $(shell ${KNOCONFIG} version)
KNO_MAJOR	 := $(shell ${KNOCONFIG} major)
KNO_MINOR	 := $(shell ${KNOCONFIG} minor)
PKG_VERSION      := $(shell u8_gitversion ./etc/knomod_version)
PKG_MAJOR        := $(shell cat ./etc/knomod_version | cut -d. -f1)
FULL_VERSION     := ${KNO_MAJOR}.${KNO_MINOR}.${PKG_VERSION}
U8LIBINSTALL	 := $(shell which u8_install_shared 2>/dev/null || echo u8_install_shared)

INIT_CFLAGS      := ${CFLAGS}
INIT_LDFLAGS     := ${LDFLAGS}
CFLAGS		  = ${INIT_CFLAGS} ${PACKAGE_CFLAGS} ${KNO_CFLAGS} ${XCFLAGS}
LDFLAGS		  = ${INIT_LDFLAGS} ${PACKAGE_LDFLAGS} ${KNO_LDFLAGS} ${XCFLAGS}

PKG_NAME	 := imagetools

SUDO             := $(shell which sudo)

MKSO		  = $(CC) -shared $(CFLAGS) $(LDFLAGS) $(LIBS)
MSG		  = echo
SYSINSTALL        = /usr/bin/install -c
MACLIBTOOL	  = $(CC) -dynamiclib -single_module -undefined dynamic_lookup \
			$(LDFLAGS)

GPGID             = FE1BC737F9F323D732AA26330620266BE5AFF294
CODENAME	 := $(shell ${KNOCONFIG} codename)
REL_BRANCH	 := $(shell ${KNOBUILD} getbuildopt REL_BRANCH current)
REL_STATUS	 := $(shell ${KNOBUILD} getbuildopt REL_STATUS stable)
REL_PRIORITY	 := $(shell ${KNOBUILD} getbuildopt REL_PRIORITY medium)
ARCH             := $(shell ${KNOBUILD} getbuildopt BUILD_ARCH || uname -m)
APKREPO          := $(shell ${KNOBUILD} getbuildopt APKREPO /srv/repo/kno/apk)
APK_ARCH_DIR      = ${APKREPO}/staging/${ARCH}
RPMDIR		  = dist

%.o: %.c makefile .buildmode
	$(CC) $(CFLAGS) -D_FILEINFO="\"$(shell u8_fileinfo ./$< $(dirname $(pwd))/)\"" -o $@ -c $<
	@$(MSG) CC $@ $<
%.so: %.o
	$(MKSO) $(LDFLAGS) -o $@ $^ ${LDFLAGS}
	@$(MSG) MKSO  $@ $<
	@ln -sf $(@F) $(@D)/$(@F).${KNO_MAJOR}
%.dylib: %.o makefile
	@$(MACLIBTOOL) -install_name \
		`basename $(@F) .dylib`.${KNO_MAJOR}.dylib \
		${CFLAGS} ${LDFLAGS} -o $@ $(DYLIB_FLAGS) \
		$<
	@$(MSG) MACLIBTOOL  $@ $<

# Meta targets

default build: .buildmode
	make $(shell cat .buildmode)

module: qrcode.${libsuffix} exif.${libsuffix} imagick.${libsuffix}

standard:
	make module
debugging:
	make XCFLAGS="-O0 -g3" module

.buildmode:
	echo standard > .buildmode

debug:
	echo debugging > .buildmode
	make
normal:
	echo standard > .buildmode
	make

# Utility targets

TAGS: exif.c qrcode.c imagick.c
	etags -o TAGS $<

${CMODULES}:
	install -d $@

install: build ${CMODULES}
	@for mod_name in qrcode exif imagick; do \
	  ${SUDO} ${U8LIBINSTALL} $${mod_name}.${libsuffix} ${CMODULES} ${FULL_VERSION} "${SYSINSTALL}"; \
	done;

clean:
	rm -f *.o *.so *.so.* *.dylib

fresh:
	make clean
	make default

gitup gitup-trunk:
	git checkout trunk && git pull
